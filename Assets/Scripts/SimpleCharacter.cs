﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCharacter : MonoBehaviour
{
    Transform awakingTransform;
    
    public float currentAngle_X = 90f;
    public float currentAngle_Y = 0f;
    public float currentAngle_Z = 0f;

    void Awake() {
        awakingTransform = this.gameObject.transform;    
    }

    void changeAngel(){
        this.transform.eulerAngles = new Vector3(
            currentAngle_X,
            currentAngle_Y,
            currentAngle_Z
        );
    }


    IEnumerator Start()
    {
        //first time change angle based on initial angle in inspector
        changeAngel();

        print(this.gameObject+" | Starting " + Time.time);
        yield return StartCoroutine(WaitAndPrint(0.1f));
        print(this.gameObject+" | Done " + Time.time);
    }

    // suspend execution for waitTime seconds
    IEnumerator WaitAndPrint(float waitTime)
    {
        while(this.transform.eulerAngles.x != this.awakingTransform.rotation.x){
            yield return new WaitForSeconds(waitTime);
            this.currentAngle_X -= 5f;
            changeAngel();
            // print("WaitAndPrint " + Time.time);

        }
    }
}